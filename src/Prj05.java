import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Prj05 {
    public static void main(String[] args) {
        try {
            BufferedReader br = new BufferedReader(new FileReader("D:\\test\\1.txt"));
            String line;
            List<String> list = new ArrayList<>();
            while ((line = br.readLine()) != null){
                list.add(line);
            }
            Collections.sort(list);
            for (String word : list){
                System.out.println(word);
            }
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
